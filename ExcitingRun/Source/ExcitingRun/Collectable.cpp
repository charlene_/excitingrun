// Fill out your copyright notice in the Description page of Project Settings.

#include "ExcitingRun.h"
#include "Collectable.h"


// Sets default values
ACollectable::ACollectable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACollectable::BeginPlay()
{
	Super::BeginPlay();
	pawn=UGameplayStatics::GetPlayerPawn(this, 0);
}

// Called every frame
void ACollectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    FVector actorLocation =pawn->GetActorLocation();
    FVector itemLocation = GetActorLocation();
    float distance = FVector::Dist(actorLocation, itemLocation);
    if(distance<220){
        Destroy();
    }

}

